import time
import pandas
import logging
import os
import re

from bs4 import BeautifulSoup
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from pymongo import MongoClient


class Scrapper:
    OUTPUT_ROOT_DIR = 'output'
    RESOURCES_ROOT_DIR = 'resources'
    SPREADSHEET_FILENAME = 'pesquisas_prontas_v2.csv'
    NEW_SPREADSHEET_FILENAME = 'scrapper_stf.csv'
    DATABASE_URL = 'localhost:27017'
    INTERVAL_TO_MOUNT_HTML = 5
    INTERVAL_TO_CHANGE_REQUEST = 7.5

    def __init__(self):
        self.selenium = None
        self.total_per_url = 0
        self.current_page = 0
        self.page_count = 0
        self.current_url = 1
        self.acordaos_per_page = 10
        self.spreadsheet_dict = []
        self.empty_url_list = []
        self.acordaos = []
        self.db = MongoClient(self.DATABASE_URL)
        self.resources_filepath = f'{self.RESOURCES_ROOT_DIR}/{self.SPREADSHEET_FILENAME}'
        self.output_filepath = f'{self.OUTPUT_ROOT_DIR}/{self.NEW_SPREADSHEET_FILENAME}'

    def run(self):
        self.__setup_logging()
        self.__setup_webdriver()
        self.__build_url_list_from_spreadsheet()
        self.__start_scrapper()

    @staticmethod
    def __setup_logging():
        console_handler = logging.StreamHandler()
        lineformat = '[%(asctime)s] | %(levelname)s | [%(process)d - %(processName)s]: %(message)s'
        dateformat = '%d-%m-%Y %H:%M:%S'
        logger_level = 10
        logging.basicConfig(format=lineformat, datefmt=dateformat, level=logger_level, handlers=[console_handler])
        logging.info(f'Logger configurado com sucesso.')

    def __setup_webdriver(self):
        browser_driver = ChromeDriverManager().install()
        self.selenium = webdriver.Chrome(browser_driver)
        logging.info(f'Selenium Webdriver configurado com sucesso.')

    def __build_url_list_from_spreadsheet(self):
        root_path = os.getcwd()
        complete_path = f'{root_path}/{self.resources_filepath}'
        spreadsheet_dict = pandas.read_csv(complete_path).to_dict()
        self.spreadsheet_dict = [{'area': spreadsheet_dict["area"][index], 'tema': spreadsheet_dict["tema"][index], 'discussao': spreadsheet_dict["discussao"][index], 'url': spreadsheet_dict["url"][index]} for index in range(240)]
        logging.info(f'URLS extraidas da planilhas "pesquisa_prontas.csv" com sucesso.')

    def __start_scrapper(self):
        logging.info(f'Iniciando scrapper no portal de jurisprudência do STF.')
        for item in self.spreadsheet_dict:
            try:
                self.selenium.get(item['url'])
                time.sleep(self.INTERVAL_TO_MOUNT_HTML)
                self.__get_totals(self.__get_main_container_from_body(self.selenium.page_source))
                self.__scrapy_pages_from_url(item)
                self.__update_scrapper_counters()
                time.sleep(self.INTERVAL_TO_MOUNT_HTML)
            except Exception as err:
                logging.error(err)
                self.__empty_url_handler(item['url'])
        self.selenium.close()
        self.selenium.quit()
        self.__create_metadata_spreadsheet()
        self.__check_list_of_empty_urls()
        self.db.juridics.metadata.insert_many(self.acordaos)

    def __get_totals(self, main_content):
        total_content = main_content.find('div', {'class': 'mb-15'}).find('span', {'class': 'ng-star-inserted'}).text
        total_result = re.findall(r'\d+', total_content)[0]
        self.total_per_url = int(total_result)
        self.page_count = (int(self.total_per_url) // self.acordaos_per_page)
        logging.info(f'Foi encontrado {self.total_per_url} resultado(s) em {self.page_count} página(s) para a URL número {self.current_url}.')

    @staticmethod
    def __get_main_container_from_body(body):
        html = BeautifulSoup(body, 'html.parser')
        app_home = html.find('app-home').find('main').find('search')
        container = app_home.find('div', {'class': 'main-container ng-star-inserted'}).find('div')
        return container.find('div', {'fxflex': '0 1 62%'})

    def __scrapy_pages_from_url(self, item):
        logging.info(f'Iniciando consulta da URL {self.current_url}.')
        while self.__has_next_page():
            main_container = self.__get_main_container_from_body(self.selenium.page_source)
            self.__scrape_data_from_html(main_container, item)
            self.__increment_page()
            self.__go_to_next_page()

    def __has_next_page(self):
        return (self.current_page + 1) <= self.page_count

    def __scrape_data_from_html(self, main_content, spreadsheet_item):
        logging.info(f'Coletando dados da página {self.current_page + 1}.')
        for acordao_div in main_content.findAll('div', {'class': 'result-container jud-text p-15 ng-star-inserted'}):
            acordao_dict = {}
            acordao_main_content = acordao_div.find('div', {'id': 'result-principal-header'})
            acordao_main_headers = acordao_main_content.findAll('h4', {'class': 'ng-star-inserted'})
            acordao_dates_div = acordao_main_content.find('span', {'fxlayout.gt-xs': 'row'}).findAll('h4', {'class': 'ng-star-inserted'})
            acordao_ementa_div = acordao_div.find('div', {'id': 'other-occurrences'})
            acordao_badge = acordao_div.find('div', {'class', 'badge p-5 featured-style'})
            acordao_dict['TÍTULO'] = acordao_div.find('a', {'class': 'ng-star-inserted'}).find('h4', {'class': 'ng-star-inserted'}).text
            acordao_dict['ORGÃO JULGADOR'] = acordao_main_headers[0].span.text.strip()
            acordao_dict['RELATOR'] = acordao_main_headers[1].span.text.strip()
            acordao_dict['DATA JULGAMENTO'] = acordao_dates_div[0].span.text.strip()
            acordao_dict['DATA PUBLICAÇÃO'] = acordao_dates_div[1].span.text.strip()
            acordao_dict['EMENTA'] = acordao_ementa_div.find('p', {'class': 'jud-text m-0'}).text.strip()
            if bool(acordao_badge) is True:
                acordao_dict['REPERCURSSãO GERAL'] = (acordao_badge.text.strip().split())[-1]
            else:
                acordao_dict['REPERCURSSãO GERAL'] = 'Não possui'
            acordao_dict['ÁREA'] = spreadsheet_item['area'].strip()
            acordao_dict['TEMA'] = spreadsheet_item['tema'].strip()
            acordao_dict['DISCUSSÃO'] = spreadsheet_item['discussao'].strip()
            acordao_dict['URL'] = spreadsheet_item['url'].strip()
            if acordao_dict not in self.acordaos:
                self.acordaos.append(acordao_dict)

    def __increment_page(self):
        self.current_page += 1

    def __go_to_next_page(self):
        logging.info(f'Alterando scrapper para a página {self.current_page + 1} da URL {self.current_url}.')
        try:
            list_button_index = self.selenium.find_elements_by_tag_name('li')
            self.selenium.find_element_by_xpath(f'//*[@id="dg-pagination"]/ul/li[{len(list_button_index) -1}]').click()
            time.sleep(self.INTERVAL_TO_CHANGE_REQUEST)
        except Exception as err:
            logging.error(err)

    def __update_scrapper_counters(self):
        logging.info('Atualizando contadores.')
        self.current_url += 1
        self.total_per_page = 0
        self.current_page = 0
        self.page_count = 0

    def __empty_url_handler(self, url):
        logging.error(f'A URL {self.current_url} não retornou nenhum conteúdo. Seguindo para o próximo link.')
        self.__update_scrapper_counters()
        self.empty_url_list.append(url)

    def __create_metadata_spreadsheet(self):
        logging.info('Gerando planilha com metadados coletados.')
        pandas.DataFrame(self.acordaos).to_csv(self.output_filepath, sep='|', encoding='utf-8-sig', index=False)

    def __check_list_of_empty_urls(self):
        if len(self.empty_url_list) >= 1:
            with open(f'{self.OUTPUT_ROOT_DIR}/empty_url_list.txt', 'w') as file:
                for url in self.empty_url_list:
                    file.write(url)
                    file.write('\n')
            logging.info(f'O scrapper foi concluido com {len(self.empty_url_list)} erros de consulta. Uma arquivo de texto foi gerado com as urls quebradas.')
        else:
            logging.info('O scrapper foi concluido com sucesso.')
